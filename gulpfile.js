var nunjucksRender = require('gulp-nunjucks-render');
var gulp = require('gulp');
var data = require('gulp-data');
var merge = require('gulp-merge-json');
var htmlbeautify = require('gulp-html-beautify');
var cleanhtml = require('gulp-cleanhtml');

function mergeJSON() {
    return gulp.src('templates/data/**/*.json')
        .pipe(merge({
            fileName: 'data.json'
        }))
        .pipe(gulp.dest('./templates'));
}

function nunjucks() {
     // Gets .html and .nunjucks files in pages
     return gulp.src('templates/pages/**/*.+(html|nunjucks)')
     .pipe(data(function() {
         return require('./templates/data.json')
     }))
     .pipe(nunjucksRender({
         path: ['templates'],
         trimBlocks: true,
         lstripBlocks : true,
         noCache: true,
         ext: '.html'
         }))
     .pipe(gulp.dest('public'));
}

function beautifyHTML() {
    var options = [
        {
            indentSize: 4,
            indent_char: " "
        }
    ];

    return gulp.src('./public/*.html')
          .pipe(htmlbeautify(options))
          .pipe(gulp.dest('./public/'))
}

function cleanHTML() {
    return gulp.src('./public/*.html')
        .pipe(cleanhtml())
        .pipe(gulp.dest('./public/'));
}

// exports.build = gulp.series(mergeJSON, nunjucks, beautifyHTML);
exports.default = function() {
    // You can use a single task
    gulp.watch([
        'templates/**/*.+(html|nunjucks)',
        'templates/pages/**/*.+(html|nunjucks)',
        'templates/partials/**/*.+(html|nunjucks)',
        'templates/data/**/*.json'
    ], gulp.series(mergeJSON, nunjucks, beautifyHTML))    
};
  
  