# Description

Personal starter kit for slice design to HTML, use nunjucks as template engine and use gulp to build the HTML.

## Installation

Install `gulp` first:

```bash
npm install -g gulp
```
Install gulp nunjucks

```bash
npm install
```

To watch run:

```bash
gulp
```

every time we change the nunjucks file or the data JSON. and this command will generated to public folder. All assets files should place in folder `public/assets`

## Folders Structure

1. **Templates**: All nunjucks files place in this folder
2. **Data**: JSON data for some pages, so on nunjucks files we can loop this JSON data
3. **Pages**: Files in this folder will be generated as HTML and become page we can access
4. **Partials**: For partials files

## Reference
* [Nunjucks Documentation](https://mozilla.github.io/nunjucks/templating.html)
* [Gulp Nunjucks](https://www.npmjs.com/package/gulp-nunjucks)