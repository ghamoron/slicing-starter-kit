Pace.on('start', function(){
    $('.pace').wrap('<div class="pace-wrap"></div>');
});

Pace.on('done', function(){
    $('.pace-wrap').remove();
});

Pace.start();